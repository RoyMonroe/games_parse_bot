import requests
import json
from parser import *
from flask import (
    Flask, request, jsonify
)
from config import *

"""
TODO: 1.Спарсить словарь со всеми командами и проверять входящее сообщение \на наличие названия команды в запросе. 
TODO: 2. Сделать с заменами например: vp, virtuspro, виртуспро, виртус про, вп и т.д. на virtus-pro
"""
# https://a0990b41.ngrok.io

app = Flask(__name__)
URL = f'https://api.telegram.org/bot{BOT_TOKEN}/'
# app = Flask(__name__)


def write_json(data, filename='answer.json'):
    with open(filename, 'w') as file:
        json.dump(data, file, indent=2, ensure_ascii=False)


def send_message(chat_id, text):
    url = URL+'sendMessage'
    answer = {
        'chat_id': chat_id,
        'text': text
    }
    r = requests.post(url, json=answer)
    return r.json()


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        r = request.get_json()
        chat_id = r['message']['chat']['id']
        message = r['message']['text']
        message = message.replace(" ", "-")
        url = base_url+message
        par = teams_pars(base_url=url, header=header)
        par=json.dumps(par)
        par=re.sub('["\[{}\].()!@#$]', '', par)
        par=par.replace(',', '\n')
        print(type(par))
        send_message(chat_id, text=par)

        write_json(r)
        return jsonify(r)
    else:
        return "<h1>Hi, i'm bot</h1>"


if __name__ == "__main__":
    app.run()
