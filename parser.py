import requests, fake_useragent, re
from bs4 import BeautifulSoup as bs

# Random User-Agent
ua = fake_useragent.UserAgent()
user = ua.random
header = {
    "user-agent": str(user)
}
# input = str(input("Введите команду: ")).replace(" ", "-")
base_url = 'https://game-tournaments.com/dota-2/team/'


def teams_pars(base_url, header):
    session = requests.Session()
    request = session.get(base_url, headers=header)
    if request.status_code == 200:
        soup = bs(request.content, 'lxml')
        current_table = soup.find('div', {'id': 'block_matches_current'})
        pairs = current_table.find_all(
            'a', {'class': 'mlink'}, title=True
        )
        times = current_table.find_all(
            'span', {'class': 'sct'}
        )
        tournaments = current_table.find_all(
            'a', {'class': 'ta odtip'}, title=True
        )
        data = {}
        for cir, pair in enumerate(pairs):
            for time in times:
                time = times[cir]
                for tour in tournaments:
                    tour = tournaments[cir]['title']
                    pair = re.sub('[1234567890,.()!@#$\n]', '', pair.text)
                    title = " ".join(pair.split())
                    time = " ".join(time.text.split())
                    data[title] = time, tour
                    if cir >= 0:
                        break
                if cir >= 0:
                    break
    else:
        return 'Ошибка соединения с сервером\nПопробуйте еще раз'
    if not data:
        return 'На ближайшее время игр нет'
    else:
        return data
    
